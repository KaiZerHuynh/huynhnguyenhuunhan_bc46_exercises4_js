//Bài 1
var tong = 0;
var n = 1;
while(tong <= 10000)
{
    n++;
    tong +=n;
    document.getElementById("result").innerHTML =`Số nguyên dương nhỏ nhất là: ${n}`; 
}
//Bài 2
function tinhTong()
{
    var sum = 1;
    var tong = 0;
    var soX = document.getElementById("numberX").value*1;
    var soN = document.getElementById("numberN").value*1;
    for(var i = 1;i <= soN;i++){
        sum *= soX;
        tong += sum; 
    }
    document.getElementById("result1").innerHTML =`Tổng của số là: ${tong}`;
}
//Bài 3
function tinhGiaiThua(){
    var tong = 1;
    var giaiThua = document.getElementById("giaiThua").value*1;
    for(var i = 1; i <= giaiThua;i++)
    {
        tong *= i;
    }
    document.getElementById("result2").innerHTML =`Tổng của số là: ${tong}`;
}
//Bài 4
function In(){
    var content ='';
    var contentHTML = "";
    for(var i = 1; i <= 10;i++)
    {
	    if (i % 2 == 0) {
			content = `<p style ="background:red; color:white">Div số chẵn là: ${i}</p>`;
		} 
        else{
			content = `<p style ="background:blue; color:white">Div số lẻ là: ${i}</p>`;
		}
        contentHTML +=content;
    } 
	document.getElementById("result3").innerHTML = contentHTML;
}